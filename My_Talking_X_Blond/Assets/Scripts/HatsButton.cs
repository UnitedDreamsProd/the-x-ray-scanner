﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HatsButton : MonoBehaviour {


    public GameManager gm;
    public int desiredValue;
    public Image ImageBigHatsUI;
    public Text childText;
    public GameObject childButton;
    public int index;
    public Image LockIcon;
    public Image CheckMark;
    public Image[] forDisabledImage;

    Sprite chooseHats;



    Button _button;

    void Start()
    {
        _button = GetComponent<Button>();
        chooseHats = GetComponent<Image>().sprite;
    }






    void Update()
    {
        if (gm.countCoins >= desiredValue)
            LockIcon.enabled = false;
        else
            LockIcon.enabled = true;



        if (gm.alHats[index - 1] == ImageBigHatsUI.sprite && gm.ImageBigHatsUI.isActiveAndEnabled)
        {
            CheckMark.enabled = true;
            LockIcon.enabled = false;
        }
        else
            CheckMark.enabled = false;

        
    }




    public void SelectHats()
    {
        int interimParam = gm.countCoins;

        if (interimParam >= desiredValue)
        {
           

            //если шапка уже куплена
            if (gm.BuyHats.Contains(chooseHats))
            {
                ImageBigHatsUI.enabled = true;
                ImageBigHatsUI.sprite = chooseHats;
                gm.indexNeenHats = index - 1;
                CheckMark.enabled = true;
                disEnableMasImages();
                childButton.SetActive(true);
                StartCoroutine(showChildText());

            }
            //если нет вызываем метод для покупки
            else
            {
                childText.enabled = true;
                childText.transform.position =
                     new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 28);
                StartCoroutine(showChildButton());

            }
            
            

        }
        else
            StartCoroutine(showChildText());
    }


    private IEnumerator showChildText()
    {
        childText.transform.position =
            new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 28);
        childText.enabled = true;
        childText.text = "" + desiredValue;
        yield return new WaitForSeconds(0.5f);
        childText.enabled = false;
        childButton.SetActive(false);
    }


    private IEnumerator showChildButton()
    {
        childText.transform.position =
            new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 28);
        childText.text = "" + desiredValue;
        childButton.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        childButton.SetActive(false);
        childText.enabled = false;

    }

    //Покупка шапки
    public void WearHat()
    {

        Sprite chooseHats = GetComponent<Image>().sprite;
        ImageBigHatsUI.enabled = true;
        ImageBigHatsUI.sprite = chooseHats;
        gm.indexNeenHats = index - 1;
        gm.BuyHats.Add(chooseHats);
        gm.countCoins -= desiredValue;
        gm.textCoins.text = "" + gm.countCoins;
        CheckMark.enabled = true;
        disEnableMasImages();






    }



    //показ и сокрытие галочек
    void disEnableMasImages()
    {
        for (int i = 0; i < forDisabledImage.Length; i++)
        {
            forDisabledImage[i].enabled = false;
        }
    }

}
