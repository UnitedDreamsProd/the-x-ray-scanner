﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsScript : MonoBehaviour {


    public GameObject enabledMusic;
    public GameObject disabledMusic;
    public GameObject enabledSound;
    public GameObject disabledSound;

    
    bool isPruf = false;

   

    


    public void IsDownTouch()
    {
        isPruf = !isPruf;
        if (isPruf)
        {  
            enabledMusic.SetActive(true);
            enabledSound.SetActive(true);
        }
        else
        {
            enabledMusic.SetActive(false);
            disabledMusic.SetActive(false);
            enabledSound.SetActive(false);
            disabledSound.SetActive(false);
        }
    }

    public void isMusicOn()
    {
        enabledMusic.SetActive(false);
        disabledMusic.SetActive(true);
        SoundManager.instance.musicAudio.Pause();
    }

    public void isMusicOff()
    {
        enabledMusic.SetActive(true);
        disabledMusic.SetActive(false);
        SoundManager.instance.musicAudio.Play();
    }


    public void isSoundOn()
    {
        enabledSound.SetActive(false);
        disabledSound.SetActive(true);
        SoundManager.instance.effectsAudio.enabled = false;
        SoundManager.instance.playersAudio.enabled = false;
    }

    public void isSoundOff()
    {
        enabledSound.SetActive(true);
        disabledSound.SetActive(false);
        SoundManager.instance.effectsAudio.enabled = true;
        SoundManager.instance.playersAudio.enabled = true;
    }




}
