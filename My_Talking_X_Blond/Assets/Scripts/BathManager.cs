﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BathManager : MonoBehaviour {

    public AudioClip bathMusic;
    public GameObject hadnsMirror;
    public GameObject hands;
    public GameObject waterOn;
    public GameObject waterEffects;
    public GameObject waterEffectsMirror;


    private float delayTimes = 0;
    private GameObject gmObject;
    private GameManager gm;
    [SerializeField]private int countTap = 0;

    [SerializeField]
    int countToNeed = 3;


    void Start()
    {
        //ищем в сцене Игровой менеджер и получаем доступ к скрипту
        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();

       
    }


    void Update()
    {
        delayTimes += Time.deltaTime;
        if (delayTimes >= 3)
        {
            delayDisableSprites();
        }
    }


  
    public void isDownTouch()
    {
        if (!SoundManager.instance.effectsAudio.isPlaying)
            SoundManager.instance.PlaySingle(bathMusic);

        hadnsMirror.SetActive(true);
        hands.SetActive(true);
        waterOn.SetActive(true);
        waterEffects.SetActive(true);
        waterEffectsMirror.SetActive(true);

        countTap++;
        if (countTap == countToNeed)
        {
            gm.InsertCountCoins(0);
            countTap = 0;
        }

        delayTimes = 0;


    }





    private void delayDisableSprites()
    {
        hadnsMirror.SetActive(false);
        hands.SetActive(false);
        waterOn.SetActive(false);
        waterEffects.SetActive(false);
        waterEffectsMirror.SetActive(false);
    }


}
