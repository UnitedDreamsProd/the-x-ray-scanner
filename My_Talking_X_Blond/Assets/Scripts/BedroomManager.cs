﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BedroomManager : MonoBehaviour {

    public AudioClip bedroomSound;

    private float delayTimes = 0;
    private bool SleepFaceBool = false;
    private GameObject gmObject;
    private GameManager gm;
    [SerializeField]private int countTap = 0;

    [SerializeField]
    int countToNeed = 3;

    public Image sleepFace;
    public Text SleepText;


    void Start()
    {
        //ищем в сцене Игровой менеджер и получаем доступ к скрипту
        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();

       
    }

    void Update()
    {
        delayTimes += Time.deltaTime;
        if (delayTimes >= 3)
        {
            sleepFace.enabled = false;
            SleepText.enabled = false;
        }
    }

    

    public void isDownTouch()
    {
        /*if (!SoundManager.instance.effectsAudio.isPlaying)
            SoundManager.instance.PlaySingle(bedroomSound);*/

        sleepFace.enabled = true;
        SleepText.enabled = true;

        countTap++;
        if (countTap == countToNeed)
        {
            gm.InsertCountCoins(3);
            countTap = 0;
        }

        delayTimes = 0;

    }

    


}
