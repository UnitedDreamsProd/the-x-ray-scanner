﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FunManager : MonoBehaviour {


    public AudioClip funSound;
    public Transform PointInstantiateCoin;

   
    private GameObject gmObject;
    private GameManager gm;
    [SerializeField]private int countTap = 0;
	[SerializeField] private GameObject bonusCoin = null;
	[SerializeField] private GameObject videoCoin = null;

    [SerializeField]
    int countToNeed = 3;
    [SerializeField]
    int bpmBonus = 200;

    public Image HappyFace;
    public FaceControlManager FCM;


    void Start()
    {
        //ищем в сцене Игровой менеджер и получаем доступ к скрипту
        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();

       
    }


    public void isDownTouch()
    {
        if (!SoundManager.instance.effectsAudio.isPlaying)
            SoundManager.instance.PlaySingle(funSound);

        if (!FCM.SadFace)
            HappyFace.enabled = true;

        countTap++;
        if (countTap % countToNeed == 0)
        {
            gm.InsertCountCoins(2);            
        }


        if (countTap % bpmBonus == 0)   
        {
            int randomChisl = Random.Range(0, 2);

            if (randomChisl == 0)
            {
                int advertisingOrCoins = Random.Range(0, 2);

                if (advertisingOrCoins == 0 && bonusCoin == null)
                {
					bonusCoin = Instantiate(Resources.Load("CoinBonus"), PointInstantiateCoin) as GameObject;

                }
                else
                {
					if (videoCoin == null)
						videoCoin = Instantiate(Resources.Load("VideoBonus"), PointInstantiateCoin) as GameObject;
                }

            }
        }

    }


    public void isUpTouch()
    {
        HappyFace.enabled = false;
    }


}
