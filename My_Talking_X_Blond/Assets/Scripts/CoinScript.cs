﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour {

    private GameObject gmObject;
    private GameManager gm;
    

    public AudioClip bonusSound;
    


    void Awake()
    {
        
     


        //ищем в сцене Игровой менеджер и получаем доступ к скрипту
        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();
    }

    void OnMouseDown()
    {
        gm.countCoins++;
        gm.textCoins.text = "" + gm.countCoins;
        Destroy(gameObject);

        SoundManager.instance.PlaySingle(bonusSound);
    }

    

}
