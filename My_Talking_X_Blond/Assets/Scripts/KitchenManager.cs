﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenManager : MonoBehaviour {


    public AudioClip eatSound;
    public GameObject[] fruits;

    
    private GameObject gmObject;
    private GameManager gm;
    [SerializeField]private int countTap = 0;

    [SerializeField]
    int countToNeed = 3;


    void Start()
    {
        
        int randChisl = Random.Range(0, 3);
        fruits[randChisl].SetActive(true);

        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();

       

    }


    public void isDownTouch()
    {
        if (!SoundManager.instance.effectsAudio.isPlaying)
            SoundManager.instance.PlaySingle(eatSound);

        countTap++;
        if (countTap == countToNeed)
        {
            gm.InsertCountCoins(4);
            countTap = 0;
        }

    }



}
