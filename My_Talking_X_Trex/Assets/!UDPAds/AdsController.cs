﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;

public class AdsController : MonoBehaviour {

	private static AdsController instance;

	private bool useUnityAds = true;

	//public float unityAdsChance = 50;

	public int unityAdsGameID = 61740;
	
	public string unityAdsDefaultZone = "defaultZone";

	public string unityAdsRewardedZone = "rewardedVideoZone";

	public bool atTop = true;
	public bool smartBanner = false;
	public string topBannerUnitIdAndroid;
	public string topBannerUnitIdIOS;
	private BannerView topBanner;

	public string largeBannerUnitIdAndroid;
	public string largeBannerUnitIdIOS;
	private InterstitialAd largeBanner;

	//public string mixedBannerUnitIdAndroid;
	//public string mixedBannerUnitIdIOS;
	//private InterstitialAd mixedBanner;

	public bool useTopBanner = true;
	public bool useLargeBanners = true;
	//public bool useMixedBanners = true;

	public delegate void OnVideoFinished();
	
	public OnVideoFinished onVideoFinished;

	private bool showing = false;
	//private float showingTimer = 60;

	[HideInInspector]
	public List<string> scenesList;

	[HideInInspector]
	public List<bool> showAds;

	[HideInInspector]
	public List<bool> showTopBanner;

	[HideInInspector]
	public bool showAdsOnLoad = true;

	private int currentScene = 0;

	public static AdsController Instance()
    {

        if (instance == null)
        {

            GameObject go = new GameObject("AdsController");
			instance = go.AddComponent<AdsController>();
        }

        return instance;
    }

    void Awake()
    {
		
        if (instance != null && instance != this) {
			DestroyImmediate(this.gameObject);
			return;
		}
        else instance = this;

		DontDestroyOnLoad (instance.gameObject);

		if(useTopBanner) ShowTopBanner ();
		
		if(useLargeBanners) InitLargeBanner ();

		//if(useMixedBanners) InitMixedBanner ();

		Log ("Awake");
    }

//----------------------------------------------------------------------------------------------------------------------

	public void InitializeTopBanner() {
	
		if (topBanner != null) {
			Log("Destroying top banner");
			topBanner.Destroy ();
		}

		#if UNITY_ANDROID
		string topBannerUnitId = topBannerUnitIdAndroid;
		#elif UNITY_IPHONE
		string topBannerUnitId = topBannerUnitIdIOS;
		#else
		string topBannerUnitId = "unexpected_platform";
		#endif
		
		
		// Create a 320x50 banner at the top of the screen.
		AdPosition position;
		if (atTop) position = AdPosition.Top;
		else position = AdPosition.Bottom;
		if(smartBanner)
			topBanner = new BannerView(topBannerUnitId, AdSize.SmartBanner, position);
		else
			topBanner = new BannerView(topBannerUnitId, AdSize.Banner, position);
		// Create an empty ad request.
		//AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).Build();
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		topBanner.LoadAd(request);
		
		topBanner.AdLoaded += TopBannerLoaded;
		topBanner.AdOpened += TopBannerOpened;
		topBanner.AdFailedToLoad += TopBanneFailedToLoad;
		topBanner.AdClosed += TopBannerClosed;
	}

	public void TopBannerLoaded(object sender, EventArgs args) {

		Log ("Banner loaded");

		if(currentScene < showTopBanner.Count) {
			if(showTopBanner[currentScene]) 
				ShowTopBanner();
			else
				HideTopBanner();
		} else topBanner.Show ();
	}

	public void TopBannerOpened(object sender, EventArgs args) {

		//Log ("Banner opened");
	}

	public void TopBanneFailedToLoad(object sender, AdFailedToLoadEventArgs args) {

		ShowTopBanner ();
		//Log("Banner failed to load");
	}

	public void TopBannerClosed(object sender, EventArgs args) {

		ShowTopBanner ();
		//Log ("Banner closed");
	}

	public void ShowTopBanner() {

		if (topBanner == null) {
			InitializeTopBanner();
		}

		topBanner.Show ();
	}

	public void HideTopBanner() {
	
		if (topBanner == null) {
			InitializeTopBanner();
		}
		
		topBanner.Hide ();
	}
//-----------------------------------------------------------------------------------------------------------------

	public void InitLargeBanner() {
			
		#if UNITY_ANDROID
		string bannerUnitId = largeBannerUnitIdAndroid;
		#elif UNITY_IPHONE
		string bannerUnitId = largeBannerUnitIdIOS;
		#else
		string bannerUnitId = "unexpected_platform";
		#endif

		largeBanner = new InterstitialAd (bannerUnitId);

		largeBanner.AdLoaded += LargeBannerLoaded;
		largeBanner.AdOpened += LargeBannerOpened;
		largeBanner.AdFailedToLoad += LargeBannerFailedToLoad;
		largeBanner.AdClosed += LargeBannerClosed;

		RequestLargeBanner ();
	}

	private bool requestLargeBanner = false;

	public void RequestLargeBanner() {
	
		requestLargeBanner = false;

		if(largeBanner == null) return;

		Log("Requesting large banner");

		AdRequest request = new AdRequest.Builder ().Build ();
		largeBanner.LoadAd (request);
	}

	private bool unityNow = true;
	public void TryToShowLargeBanner() {

        Log("TryToShowLargeBanner " + showing + " " + UnityAdsHelper.IsReady(unityAdsDefaultZone) + " " + (largeBanner != null));

		if(showing) return;

		bool showed = false;

		if (useUnityAds && unityNow && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd (unityAdsDefaultZone);
			unityNow = false;
			return;
		} else if (largeBanner != null) {
			showed = false;
			if (largeBanner.IsLoaded ()) {
				showing = true;
				largeBanner.Show ();
				showed = true;
				unityNow = true;
			}
		} else {
			unityNow = true;
		}

		if (useUnityAds && !showed && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			unityNow = false;
			return;
		}
	}

	public void LargeBannerLoaded(object sender, EventArgs args) {
		
		Log ("Large banner loaded");
	}
	
	public void LargeBannerOpened(object sender, EventArgs args) {

		//Log ("Large banner opened");
	}
	
	public void LargeBannerFailedToLoad(object sender, AdFailedToLoadEventArgs args) {

		requestLargeBanner = true;

		//Log("Large banner failed to load");
	}

	public void LargeBannerClosed(object sender, EventArgs args) {

		showing = false;

		requestLargeBanner = true;

		//Log("Large banner closed");
	}

//--------------------------------------------------------------------------------------------------------------------

	public void TryToShowVideoBanner() {

		if(showing) return;
		
		if (useUnityAds && UnityAdsHelper.IsReady (unityAdsRewardedZone)) {
			showing = true;
			UnityAdsHelper.onFinished = VideoFinished;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsRewardedZone);
			return;
		}
	}

	public void VideoFinished() {
	
		Log ("Video finished");

		showing = false;

		if(onVideoFinished != null) onVideoFinished();
	}

	public void BannerClosed() {
	
		showing = false;
	}

	public void TryToShowVideoBannerSkipable() {
		
		if(showing) return;
		
		if (useUnityAds && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			unityNow = false;
			return;
		}
	}
//-----------------------------------------------------------------------------------------------------------------

	/*public void InitMixedBanner() {
		
		#if UNITY_ANDROID
		string bannerUnitId = mixedBannerUnitIdAndroid;
		#elif UNITY_IPHONE
		string bannerUnitId = mixedBannerUnitIdIOS;
		#else
		string bannerUnitId = "unexpected_platform";
		#endif
		
		mixedBanner = new InterstitialAd (bannerUnitId);
		
		mixedBanner.AdLoaded += MixedBannerLoaded;
		mixedBanner.AdOpened += MixedBannerOpened;
		mixedBanner.AdFailedToLoad += MixedBannerFailedToLoad;
		mixedBanner.AdClosed += MixedBannerClosed;
		
		RequestMixedBanner ();
	}

	private bool requestMixedBanner = false;
	public void RequestMixedBanner() {

		requestMixedBanner = false;

		if(mixedBanner == null) return;

		Log("Requesting mixed banner");
		
		AdRequest request = new AdRequest.Builder ().Build ();
		mixedBanner.LoadAd (request);
	}
	
	public void TryToShowMixedBanner() {

		if(mixedBanner == null) return;

		if(showing) return;
		
		if (useUnityAds && UnityEngine.Random.Range(0.0f, 100.0f) < unityAdsChance && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			return;
		}
		
		bool showed = false;
		if(mixedBanner.IsLoaded()) {
			showing = true;
			mixedBanner.Show ();
			showed = true;
		}
		
		if (useUnityAds && !showed && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			showing = true;
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsDefaultZone);
			return;
		}
	}
	
	public void MixedBannerLoaded(object sender, EventArgs args) {
		
		Log ("Mixed banner loaded");
	}
	
	public void MixedBannerOpened(object sender, EventArgs args) {
		
		//Log ("Mixed banner opened");
	}
	
	public void MixedBannerFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
		
		requestMixedBanner = true;
		
		//Log("Mixed banner failed to load");
	}
	
	public void MixedBannerClosed(object sender, EventArgs args) {
		
		showing = false;

		requestMixedBanner = true;

		//Log("Mixed banner closed");
	}//*/

//----------------------------------------------------------------------------------------------------------------------

	// Use this for initialization
	void Start () {
	    
		Log ("Start");

		UnityAdsSettings settings = (UnityAdsSettings)Resources.Load("UnityAdsSettings");
		
		if (settings == null)
		{
			Debug.LogWarning("Failed to initialize Unity Ads. Settings file not found.");
			return;
		}

		settings.androidGameId = unityAdsGameID.ToString ();
		settings.iosGameId = unityAdsGameID.ToString ();

		UnityAdsHelper.Initialize();

		/*showAdsOnLoad = PlayerPrefs.GetInt ("showAdsOnLoad", 0) == 1;
		int numScenes = PlayerPrefs.GetInt ("NumScenes", 0);
		if (numScenes > 0) {
			showAds = new List<bool>();
			for(int i = 0; i < numScenes; i++) {
				bool show = PlayerPrefs.GetInt("showOnScene" + i, 0) == 1;
				showAds.Add(show);
			}
		}//*/
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.Escape)) {
		
			//ShowLargeBanner();
		}

		/*if (showing) {
		
			showingTimer -= Time.deltaTime;

			if(showingTimer <= 0) {
			
				showingTimer = 60;
				showing = false;
			}
		}//*/

		if(requestLargeBanner) InitLargeBanner ();

		//if(requestMixedBanner) InitMixedBanner ();
	}

	void OnDestroy() {

		Log ("Destroying banner");

		if(topBanner != null) topBanner.Destroy();

		if(largeBanner != null) largeBanner.Destroy();

		//if(mixedBanner != null) mixedBanner.Destroy();
	}

	void OnLevelWasLoaded(int level) {

		currentScene = level;

        //GameObject go = GameObject.FindGameObjectWithTag("Console");
        //if(go != null) console = go.GetComponent<UnityEngine.UI.Text>();

		Log ("Level loaded " + level + " " + showAdsOnLoad + " " + showAds[level]);

		if(level < showAds.Count) {
			if(showTopBanner[level]) 
				ShowTopBanner();
			else
				HideTopBanner();
		}

		if(!showAdsOnLoad) return;

		//int l = Application.loadedLevel;
		if(level < showAds.Count && !showAds[level]) return;

		TryToShowLargeBanner();
	}

	public UnityEngine.UI.Text console;

	public void Log(string str) {

		if (console != null)
			console.text += " " + str + "\n";

		Debug.Log (str);
	}

	private bool quiting = false;
	private bool coroutineRunning = false;
	void OnApplicationQuit() {

		if (!quiting) {
			Application.CancelQuit ();
			if(!coroutineRunning) StartCoroutine (DelayedQuit ());
		}
	}

	IEnumerator DelayedQuit() {

		coroutineRunning = true;
		TryToShowVideoBanner ();
		yield return new WaitForSeconds(1);
		quiting = true;
	}//*/
}
