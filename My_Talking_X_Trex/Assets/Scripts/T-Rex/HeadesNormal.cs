﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadesNormal : MonoBehaviour {

    Image mainSprite;


    private void Start()
    {
        mainSprite = GetComponent<Image>();

    }



    // Update is called once per frame
    void Update()
    {
        mainSprite.sprite = TRexColorManager.instance.HeadesNormal[TRexColorManager.instance.indexCoSlorTRex];
    }
}
