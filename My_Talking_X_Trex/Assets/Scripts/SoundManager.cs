﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource musicAudio;
    public AudioSource effectsAudio;
    public AudioSource playersAudio;

    public float lowPitch = 0.95f;
    public float highPitch = 1.05f;


    public static SoundManager instance = null;
      

    void Awake()
    {
        //патерн синглтон
        if (instance == null)            
            instance = this;       
        else if (instance != this)            
            Destroy(gameObject);
       
        DontDestroyOnLoad(gameObject);
    }
	
	
    public void PlaySingle(AudioClip clip)
    {
        float randomPitch = Random.Range(lowPitch, highPitch);
        effectsAudio.pitch = randomPitch;

        effectsAudio.clip = clip;
        effectsAudio.Play();
    }


    public void PlayerHappySounds(AudioClip clip)
    {
        float randomPitch = Random.Range(lowPitch, highPitch);
        playersAudio.pitch = randomPitch;

        playersAudio.clip = clip;
        playersAudio.Play();
    }

  

}
