﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PalleteButton : MonoBehaviour
{

    public GameManager gm;
    public int desiredValue;
    //public Image ImageBigPalleteUI;
    public Text ChildText;
    public Image LockIcon;
    public Image CheckMark;
    public Image[] forDisabledImage;

        

    Button _button;

    void Start()
    {
        _button = GetComponent<Button>();     
         


    }


    
    void Update()
    {
        if (gm.levelPlayer >= desiredValue)
            LockIcon.enabled = false;
        else
            LockIcon.enabled = true;


        if (TRexColorManager.instance.indexCoSlorTRex == desiredValue - 1)
            CheckMark.enabled = true;
        else
            CheckMark.enabled = false;

    }
    




    public void SelectColor()
    {
        int interimParam = gm.levelPlayer;
        if (interimParam >= desiredValue)
        {            
            CheckMark.enabled = true;
            disEnableMasImages();
            TRexColorManager.instance.indexCoSlorTRex = desiredValue - 1;
        }
        else
        {
            StartCoroutine(showChildText());
        }
        
            
        

    }


    private IEnumerator showChildText()
    {
        ChildText.transform.position = 
            new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 18);
        ChildText.enabled = true;
        ChildText.text = "Lvl " + desiredValue;
        yield return new WaitForSeconds(0.5f);
        ChildText.enabled = false;


    }


    void disEnableMasImages()
    {
        for (int i = 0; i < forDisabledImage.Length; i++)
        {
            forDisabledImage[i].enabled = false;
        }
    }


}




