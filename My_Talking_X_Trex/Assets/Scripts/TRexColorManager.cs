﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TRexColorManager : MonoBehaviour {

    public int indexCoSlorTRex;
    public Sprite[] BodiesTrex;
    public Sprite[] RightPows;
    public Sprite[] LeftPows;
    public Sprite[] Tailes;     //хвосты
    public Sprite[] HeadesHappy;
    public Sprite[] HeadesNormal;
    public Sprite[] HeadesSad;
    public Sprite[] HeadesSleep;

    public static TRexColorManager instance = null;


    void Awake ()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);


        indexCoSlorTRex = PlayerPrefs.GetInt("GetColorTrex", 0);

    }


    private void OnDestroy()
    {
        PlayerPrefs.SetInt("GetColorTrex", indexCoSlorTRex);
    }


	
	
}
