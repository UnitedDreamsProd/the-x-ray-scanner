﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BathManager : MonoBehaviour {

    public AudioClip bathMusic;
    public GameObject hadnsMirror;
    public GameObject hadnsMirror2;
    public GameObject hands;
    public GameObject hands2;
    public GameObject handsNormal1;
    public GameObject handsNormal2;
    public GameObject waterOn;
    public GameObject waterEffects;
    public GameObject waterEffectsMirror;


    private float delayTimes = 0;
    private GameObject gmObject;
    private GameManager gm;
    [SerializeField]private int countTap = 0;

    [SerializeField]
    int countToNeed = 3;


    void Start()
    {
        //ищем в сцене Игровой менеджер и получаем доступ к скрипту
        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();

       
    }


    void Update()
    {
        delayTimes += Time.deltaTime;
        if (delayTimes >= 3)
        {
            delayDisableSprites();
        }
    }


  
    public void isDownTouch()
    {
        if (!SoundManager.instance.effectsAudio.isPlaying)
            SoundManager.instance.PlaySingle(bathMusic);

        handsNormal1.SetActive(false);
        handsNormal2.SetActive(false);
        hadnsMirror.SetActive(true);
        hadnsMirror2.SetActive(true);
        hands.SetActive(true);
        hands2.SetActive(true);
        waterOn.SetActive(true);
        waterEffects.SetActive(true);
        waterEffectsMirror.SetActive(true);

        countTap++;
        if (countTap == countToNeed)
        {
            gm.InsertCountCoins(0);
            countTap = 0;
        }

        delayTimes = 0;


    }





    private void delayDisableSprites()
    {
        handsNormal1.SetActive(true);
        handsNormal2.SetActive(true);
        hadnsMirror.SetActive(false);
        hands.SetActive(false);
        hadnsMirror2.SetActive(false);
        hands2.SetActive(false);
        waterOn.SetActive(false);
        waterEffects.SetActive(false);
        waterEffectsMirror.SetActive(false);
    }


}
