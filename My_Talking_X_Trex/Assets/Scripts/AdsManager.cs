﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class AdsManager : MonoBehaviour
{

	private int _counter;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);

		_counter = 0;
		SceneManager.sceneLoaded += _ShowAds; // subscribe
	}

	private void _ShowAds(Scene scene, LoadSceneMode mode)
	{
		_counter++;

		if (_counter % 5 == 0)
			AdsController.Instance ().TryToShowLargeBanner ();
	}

}
