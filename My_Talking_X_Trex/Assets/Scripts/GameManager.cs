﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    
    private int[] sliderGM = new int[5];
    private Image[] _fill = new Image[5];
    public Text textCoins;
    private Text LevelText;
    public Text BadControl;
    public GameObject ScrollbarPalete;
    public GameObject ScrollbarHats;    
    
    public List<Sprite> BuyHats;
    //Все спрайты платьев и шляп, для получения доступа к ним в начале любой сцены
    
    public Sprite[] alHats;    
    public int indexNeenHats;   
    public Image ImageBigHatsUI;
    public FaceControlManager FCM;


    [HideInInspector] public int levelPlayer;
    [HideInInspector] public int countCoins;
    

       
    
    private int[] slidersPenalty = new int[] { 0, 0, 0, 0, 0 };//штраф увел. для каждого слайдера при value<30
    private float MaxSliderValue = 100;
    [SerializeField] private float wishLessTime = 0;
    [SerializeField] private float wishBadLvl = 30;
    [SerializeField] private int moneyPenalty = 500;
    [SerializeField] private int wishBack = 40;
    private int experience;
    [SerializeField] private int[] nextLevel = new int[] { 0, 150, 320, 530, 1000};
    private Slider _sliderLevelUp;
    
    


    // Use this for initialization
    void Awake()
    {

        //загрузка индекса платья и шляп для выбранных спрайтов в других сценах
        
        indexNeenHats = PlayerPrefs.GetInt("indexHat6", 0);

        //загрузка ранее выбранной одежды в начале каждой сцены
        if (indexNeenHats != 0)
        {
            ImageBigHatsUI.enabled = true;
            ImageBigHatsUI.sprite = alHats[indexNeenHats];

        }
        

        //загрузка значений потребностей персонажа из реестра
        for (int i = 0; i < sliderGM.Length; i++)
        {
            string interinIndex = i.ToString();
            sliderGM[i] = PlayerPrefs.GetInt("need3" + interinIndex, 100);
        }

        //поиск цветов слайдеров с сцене, для последующего изменения их цветов
        for (int i = 0; i < _fill.Length; i++)
        {
            string index = i.ToString();
            GameObject interimParam = GameObject.Find("Fill" + index) as GameObject;
            _fill[i] = interimParam.GetComponent<Image>();
        }

        
        //Поиск текстовго поля для количества монет
        GameObject interimParamText = GameObject.Find("TextCoinsCount");
        textCoins = interimParamText.GetComponent<Text>();

        //поиск текстового поля для уровня игрока
        GameObject interimParamLevel = GameObject.Find("LevelText");
        LevelText = interimParamLevel.GetComponent<Text>();


        //поиск текстового предупреждения о плохой игре
        GameObject interimParapBadControl = GameObject.Find("BadControl");
        BadControl = interimParapBadControl.GetComponent<Text>();



        //загрузка количества имеющихся монет
        countCoins = PlayerPrefs.GetInt("CoinsEnd6", 0);        
        textCoins.text = "" + countCoins;

        //загрузка достигнутого уровня
        levelPlayer = PlayerPrefs.GetInt("LevelEnd4", 0);
        LevelText.text = "Lvl " + levelPlayer;

        //загрузка набранного опыта
        experience = PlayerPrefs.GetInt("Exp2", 0);



        //поиск слайдера для вывода прогресса по уровню
        GameObject interimLevelUp = GameObject.Find("LevelUP") as GameObject;
        _sliderLevelUp = interimLevelUp.GetComponent<Slider>();
        Debug.Log(nextLevel.Length + "     nextLevel.length");
        Debug.Log(_sliderLevelUp.maxValue + "    _sliderLevelUp.maxValue");
        if (levelPlayer < 5)
        {
            _sliderLevelUp.maxValue = nextLevel[levelPlayer]; //levelPlayer - 1];
            _sliderLevelUp.value = experience;

        }





    }
	
	// Update is called once per frame
	void Update ()
    {
        wishLessTime += Time.deltaTime;
        if (wishLessTime >= 3f) //3f
        {
            for(int i = 0; i < sliderGM.Length; i++)  
            {
                sliderGM[i]--;
                FCM.loadValueSliders(i);



                if (sliderGM[i] <= 0)
                {
                    countCoins -= moneyPenalty;
                    textCoins.text = "" + countCoins;
                    if (levelPlayer > 1)
                    {
                        levelPlayer--;
                        LevelText.text = "Lvl " + levelPlayer;

                        experience = 0;
                        _sliderLevelUp.maxValue = nextLevel[levelPlayer]; //levelPlayer - 1];
                        _sliderLevelUp.value = 0;
                    }
                    StartCoroutine(OnBadControl());
                    sliderGM[i] = wishBack;
                    FCM.loadValueSliders(i);

                   

                }

                //начинаем отнимать монетки, если меньше 30
                if (sliderGM[i] < wishBadLvl)                
                    StartCoroutine(decrementCoins(i));

                                
            }

            InputColorScaleNeeds();

            wishLessTime = 0;
        }

        if (countCoins < 0)
        {
            countCoins = 0;
            textCoins.text = "" + countCoins;
        }

    }

    //уменьшение монет за низкие значения слайдеров
    private IEnumerator decrementCoins(int index)
    {
        

        if (countCoins > 0 )
        {
            countCoins -= slidersPenalty[index];            
            textCoins.text = "" + countCoins;
        }
                
        yield return new WaitForSeconds(1f);

        //если через секунду значение слайдера по-прежнему меньше 30, перезапускаем корутину и увеличиваем штраф
        if (sliderGM[index] < wishBadLvl)
        {
            decrementCoins(index);
            slidersPenalty[index]++;
        }

    }

    //3 секунды показываем надпись BadControl
    private IEnumerator OnBadControl()
    {
        BadControl.enabled = true;
        yield return new WaitForSeconds(3f);
        BadControl.enabled = false;
    }



    //Меняем цвет в шкалах в зависимости от процента потребности
    void InputColorScaleNeeds()
    {

        for (int i = 0; i < _fill.Length; i++)
        {
            _fill[i].color = Color.Lerp(Color.red, Color.yellow, sliderGM[i] / MaxSliderValue);
            
        }
         
    }

    //Увеличиваем процент какой-либо потребности и инкреминтируем опыт
    public void InsertCountCoins(int index)
    {
     
            sliderGM[index] += 3;
            experience += 3;
            _sliderLevelUp.value += 3;
            FCM.loadValueSliders(index);

        if (sliderGM[index] > 100)        
            sliderGM[index] = 100;
            
        
       

        
        if (levelPlayer < 5 && experience >= nextLevel[levelPlayer]) //levelPlayer - 1];
        {
            levelPlayer++;
            LevelText.text = "Lvl " + levelPlayer;
            //nextLevel.RemoveAt(0);
            experience = 0;
            if (levelPlayer < 5)
            {
                _sliderLevelUp.maxValue = nextLevel[levelPlayer];  //levelPlayer - 1];
                _sliderLevelUp.value = 0;
            }
        }
    }


    //При закрытии сцены сохранение всех необходимых данных в реестр
    void OnDestroy()
    {
        //сохранение индексов шляп        
        PlayerPrefs.SetInt("indexHat6", indexNeenHats);

        //сохранение количества имеющихся монет
        PlayerPrefs.SetInt("CoinsEnd6", countCoins);
        

        //соханение достигнутого уровня
        PlayerPrefs.SetInt("LevelEnd4", levelPlayer);
       

        //сохранение набранного опыта
        PlayerPrefs.SetInt("Exp2", experience);

        //сохранение значений потребностей персонажа
        for (int i = 0; i < sliderGM.Length; i++)
        {
            string interinIndex = i.ToString();
            PlayerPrefs.SetInt("need3" + interinIndex, sliderGM[i]);
        }

    }


    //для всех слайдеров, свойство для чтения значений потребностей
    public int[] SliderGM
    {
        get
        {
            return sliderGM;
        }
    }



    //загрузка различных уровнй
    public void LoadLevelBath()
    {
        SceneManager.LoadScene("SceneBath");
    }


    public void LoadLevelToilet()
    {
        SceneManager.LoadScene("SceneToilet");
    }


    public void LoadLevelFun()
    {
        SceneManager.LoadScene("SceneFun");
    }


    public void LoadLevelBedroom()
    {
        SceneManager.LoadScene("SceneBedroom");
    }


    public void LoadLevelKitchen()
    {
        SceneManager.LoadScene("SceneKitchen");
    }



    //открывает(скрывает) скроллбар с цветами
    public void ScrollbarStatePallete()
    {
        if (!ScrollbarPalete.activeInHierarchy)
            ScrollbarPalete.SetActive(true);
        else
            ScrollbarPalete.SetActive(false);


        if (ScrollbarHats.activeInHierarchy)        
            ScrollbarHats.SetActive(false);
        
    }



    //открывает(скрывает) скроллбар с цветами
    public void ScrollbarStateHats()
    {
        if (!ScrollbarHats.activeInHierarchy)
            ScrollbarHats.SetActive(true);
        else
            ScrollbarHats.SetActive(false);

        if (ScrollbarPalete.activeInHierarchy)
            ScrollbarPalete.SetActive(false);
        
    }


}
