﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderScript : MonoBehaviour {

    public int indexSlider;
    public GameManager gm;

    Slider _slider;

    void Start()
    {
        _slider = GetComponent<Slider>();
        
    }


    void Update()
    {
        _slider.value = gm.SliderGM[indexSlider];
        
    }


}
