﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ToiletManager : MonoBehaviour {

    public AudioClip toiletSound;
    
    private GameObject gmObject;
    private GameManager gm;
    [SerializeField]private int countTap = 0;
    [SerializeField] int countToNeed = 3;


    void Start()
    {
        //ищем в сцене Игровой менеджер и получаем доступ к скрипту
        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();

       
    }


    public void isDownTouch()
    {
        if (!SoundManager.instance.effectsAudio.isPlaying)
            SoundManager.instance.PlaySingle(toiletSound);

        countTap++;
        if (countTap == countToNeed)
        {
            gm.InsertCountCoins(1);
            countTap = 0;
        }

    }
}
