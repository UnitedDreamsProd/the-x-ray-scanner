﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoScript : MonoBehaviour {

    private GameObject gmObject;
    private GameManager gm;
    
    void Awake()
    {
        //ищем в сцене Игровой менеджер и получаем доступ к скрипту
        gmObject = GameObject.Find("GameManager");
        gm = gmObject.GetComponent<GameManager>();
    }

    void OnMouseDown()
    {
		Debug.Log ("ADS");
		AdsController.Instance ().onVideoFinished = _GetMoney;
		AdsController.Instance ().TryToShowVideoBanner ();
	}

	private void _GetMoney()
	{
		gm.countCoins += 30;
		gm.textCoins.text = "" + gm.countCoins;
		Destroy(gameObject);
	}

	void OnDestroy()
	{
		Destroy(gameObject);
	}

}
