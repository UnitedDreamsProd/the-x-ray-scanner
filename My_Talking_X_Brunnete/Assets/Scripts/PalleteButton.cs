﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PalleteButton : MonoBehaviour
{

    public GameManager gm;
    public int desiredValue;
    public Image ImageBigPalleteUI;
    public Text ChildText;
    public Image LockIcon;
    public Image CheckMark;
    public Image[] forDisabledImage;

        

    Button _button;

    void Start()
    {
        _button = GetComponent<Button>();      


    }


    
    void Update()
    {
        if (gm.levelPlayer >= desiredValue)
            LockIcon.enabled = false;
        else
            LockIcon.enabled = true;


        if (gm.indexNeenPallete == desiredValue - 1)
            CheckMark.enabled = true;
        else
            CheckMark.enabled = false;

    }
    




    public void SelectColor()
    {
        int interimParam = gm.levelPlayer;
        if (interimParam >= desiredValue)
        {
            Sprite chooseClors = GetComponent<Image>().sprite;
            if (!gm.BuyPallets.Contains(chooseClors))
            {
                
                ImageBigPalleteUI.sprite = chooseClors;
                gm.indexNeenPallete = desiredValue - 1;
                gm.BuyPallets.Add(chooseClors);
                CheckMark.enabled = true;
                disEnableMasImages();
            }
            else
            {
               
                ImageBigPalleteUI.sprite = chooseClors;
                gm.indexNeenPallete = desiredValue - 1;
                CheckMark.enabled = true;
                disEnableMasImages();
            }

        }
        else
        {
            
            StartCoroutine(showChildText());
        }
        
            
        

    }


    private IEnumerator showChildText()
    {
        ChildText.transform.position = 
            new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 18);
        ChildText.enabled = true;
        ChildText.text = "Lvl " + desiredValue;
        yield return new WaitForSeconds(0.5f);
        ChildText.enabled = false;


    }


    void disEnableMasImages()
    {
        for (int i = 0; i < forDisabledImage.Length; i++)
        {
            forDisabledImage[i].enabled = false;
        }
    }


}




