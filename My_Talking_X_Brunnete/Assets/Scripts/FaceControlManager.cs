﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class FaceControlManager : MonoBehaviour
{
    public AudioClip sadSound; 
    public AudioClip happySound;
    private bool happyBool;
    
    public GameManager gm;

    public Image Head;
    public Sprite[] masFaces;
    public int indexFormasFaces;

    private int[] masAllNeeds = new int[5];
    private bool happyFace = false;
    private bool normalFace = false;
    private bool sadFace = false;
    private int countHappyNeedFace = 0;


   
    //private int[] masLower30Value;


    void Start()
    {
        happyBool = true;


        for (int i = 0; i < masAllNeeds.Length; i++)
        {
            masAllNeeds[i] = gm.SliderGM[i];

            if (masAllNeeds[i] >= 30 && masAllNeeds[i] <= 60)
            {
                normalFace = true;
                sadFace = false;
                happyFace = false;

            }
            else if (masAllNeeds[i] < 30)
            {
                sadFace = true;
                happyFace = false;
                normalFace = false;
                int randNumb = Random.Range(0, 50);
                if (randNumb == 5)
                    SoundManager.instance.PlayerHappySounds(sadSound);
                break;
            }
            else
                countHappyNeedFace++;
        }

        if (countHappyNeedFace == 5)
        {
            happyFace = true;
            sadFace = false;
            normalFace = false;
        }

        if (happyFace)
        {
            indexFormasFaces = 0;
            Head.sprite = masFaces[indexFormasFaces];
        }
        else if (normalFace)
        {
            indexFormasFaces = 1;
            Head.sprite = masFaces[indexFormasFaces];
        }
        else
        {
            indexFormasFaces = 2;
            Head.sprite = masFaces[indexFormasFaces];
        }

    }








    void Update()
    {
        //меняем лицо в UI сцены
        if (happyFace)
        {
            indexFormasFaces = 0;
            Head.sprite = masFaces[indexFormasFaces];
        }
        else if (normalFace)
        {
            indexFormasFaces = 1;
            Head.sprite = masFaces[indexFormasFaces];
        }
        else
        {
            indexFormasFaces = 2;
            Head.sprite = masFaces[indexFormasFaces];
        }


    }


    //получаем информацию из GameManager об изменившихся потребностях,
    //подбираем нужное лицо через булевые переменные
    public void loadValueSliders(int index)
    {
        masAllNeeds[index] = gm.SliderGM[index];


        foreach (var count in masAllNeeds)
        {
            if (count >= 30 && count <= 60)
            {
                normalFace = true;
                sadFace = false;
                happyFace = false;

            }
            else if (count < 30)
            {
                sadFace = true;
                happyFace = false;
                normalFace = false;
                int randNumb = Random.Range(0, 50);                
                if (randNumb == 5)                
                    SoundManager.instance.PlayerHappySounds(sadSound);
                    
                
                break;
            }
            else
                countHappyNeedFace++;
        }

        if (countHappyNeedFace == 5)
        {
            if (happyBool)
            {
                SoundManager.instance.PlayerHappySounds(happySound);
                happyBool = false;
            }
            happyFace = true;
            sadFace = false;
            normalFace = false;
        }



    }

    public bool SadFace
    {
        get
        {
            return sadFace;
        }
    }
}





















/*
  //линк выражение для определения потребностей меньше 30
        masLower30Value = masAllNeeds.Where(c => c <= 30);

        foreach (var s in masLower30Value)
            countInt++;

        if (countInt > 0)
        {
            normalFace = true;
            countInt = 0;
        }

        //линк выражение для определения потребностей от 30 до 60
        masBeteween30To60Value = masAllNeeds.Where(c => c > 30 && c < 60);

        foreach (var s in masBeteween30To60Value)
            countInt++;

        if (countInt > 0)
        {
            normalFace = true;
            countInt = 0;
        }
*/