﻿using UnityEngine;
using System.Collections;

public class Ader : MonoBehaviour {

	public float adTime = 30;

	private float timer;

	public bool anotherMode = false;
	private int counter;

	// Use this for initialization
	void Start () {

		timer = adTime;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (timer <= 0) {

			if(anotherMode) {
				if(counter % 4 == 0) 
					AdsController.Instance().TryToShowVideoBanner();
				else
					AdsController.Instance().TryToShowVideoBannerSkipable();
				counter++;
			} else {
				AdsController.Instance().TryToShowVideoBannerSkipable();
			}
			timer = adTime;
		} else {

			timer -= Time.deltaTime;
		}
	}
}
