﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;

[CustomEditor(typeof(AdsController))]
public class AdsControllerInspector : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();

		AdsController controller = (AdsController)target;
		if(controller == null) return;

		List<string> scenesList = controller.scenesList;
		if(scenesList == null) scenesList = new List<string>();
		scenesList.Clear ();
		var scenes = UnityEditor.EditorBuildSettings.scenes;
		foreach (var scene in scenes) {
			string name = scene.path.Substring(scene.path.LastIndexOf('/')+1);
			name = name.Substring(0,name.Length-6);
			scenesList.Add(name);
		}

		List<bool> showAds = controller.showAds;
		if (showAds.Count < scenesList.Count) {
			for(int i = showAds.Count; i < scenesList.Count; i++)
				showAds.Add(true);
		}
		if (showAds.Count > scenesList.Count) {
			for(int i = showAds.Count-1; i >= scenesList.Count; i--)
				showAds.RemoveAt(i);
		}

		List<bool> showTopBanner = controller.showTopBanner;
		if (showTopBanner.Count < scenesList.Count) {
			for(int i = showTopBanner.Count; i < scenesList.Count; i++)
				showTopBanner.Add(true);
		}
		if (showTopBanner.Count > scenesList.Count) {
			for(int i = showTopBanner.Count-1; i >= scenesList.Count; i--)
				showTopBanner.RemoveAt(i);
		}

		/*for(int i = 0; i < showAds.Count; i++)
			showAds[i] = PlayerPrefs.GetInt("showOnScene" + i, 0) == 1;
        controller.showAdsOnLoad = PlayerPrefs.GetInt("showAdsOnLoad", 0) == 1;//*/

        GUILayout.Space (10);

		controller.showAdsOnLoad = EditorGUILayout.Toggle("Show ads on scene loaded", controller.showAdsOnLoad);

		GUILayout.Space (10);

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField ("Scene\tAds on reload\tBanner");
		//EditorGUILayout.LabelField ("Ads on reload");
		//EditorGUILayout.LabelField ("Banner");
		EditorGUILayout.EndHorizontal();

		GUILayout.Space (10);

		if (controller.showAdsOnLoad) {

			for (int i = 0; i < scenesList.Count; i++) {
				EditorGUILayout.BeginHorizontal();
				showAds [i] = EditorGUILayout.Toggle (scenesList [i], showAds [i]);
				showTopBanner [i] = EditorGUILayout.Toggle ("", showTopBanner [i]);
				EditorGUILayout.EndHorizontal();
			}

			PlayerPrefs.SetInt ("showAdsOnLoad", 1);

		} else PlayerPrefs.SetInt ("showAdsOnLoad", 0);

		PlayerPrefs.SetInt ("NumScenes", scenesList.Count);
		for (int i = 0; i < showAds.Count; i++) {
			if (showAds [i])
				PlayerPrefs.SetInt ("showOnScene" + i, 1);
			else
				PlayerPrefs.SetInt ("showOnScene" + i, 0);
		}
	}


}
#endif